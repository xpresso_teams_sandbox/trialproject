""" Factory class for filesystem connectors in Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import enum
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.external import AlluxioFSConnector
from xpresso.ai.core.data.connections.internal import LocalFSConnector


class FSType(enum.Enum):
    """

    Enum Class that lists types of file systems supported by FSConnector class

    """

    Alluxio = "alluxio"
    Local = "local"


class FSConnectorFactory:
    """

    Factory class to provide Connector object of specified type

    """

    @staticmethod
    def getconnector(datasource_type):
        """

        This method returns Connector object of a specific datasource

        Args:
            datasource_type (str): a string object stating the
                datasource

        Returns:
            object: Connector object
        """

        if datasource_type is None:
            return AlluxioFSConnector()
        elif datasource_type.lower() == FSType.Local.value:
            return LocalFSConnector()
        elif datasource_type.lower() == FSType.Alluxio.value:
            return AlluxioFSConnector()
        else:
            raise xpr_exp.InvalidDictionaryException
